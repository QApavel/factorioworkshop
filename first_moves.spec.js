context('Mining by hand', () => {
    beforeEach(() => {
        cy.startGame('Berlin');
    });

    it('Init Stone Production!', function () {
        for (let i = 0; i < 5; i++) {
            cy.findFirstStone().click();
            cy.mineResource();
        }
        cy.findFirstStone().click();
        cy.buildMine();

        cy.tickGame();

        cy.stoneDeposit().should('be.above', 2);
    })
});